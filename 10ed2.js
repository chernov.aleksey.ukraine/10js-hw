document.querySelectorAll('i').forEach(icon => {
    icon.addEventListener('click', elem => {
        if (elem.target.classList.contains('fa-eye')) {
            elem.target.className = 'fas fa-eye-slash icon-password';
            elem.target.parentNode.querySelector('input').type = 'text'
        } else {
            elem.target.className = 'fas fa-eye icon-password';
            elem.target.parentNode.querySelector('input').type = 'password'}
    })
});

const password = document.querySelector('.password');
const passwordConfirm = document.querySelector('.password-confirm');

document.querySelectorAll('input').forEach(item => {
    item.addEventListener('focus', () => {
        document.querySelector('.red-message').innerText = '';
    })
});

document.querySelector('form').addEventListener('submit', (event) => {
    event.preventDefault();
    if (password.value === passwordConfirm.value) {
        alert('You are welcome')
    } else {
        document.querySelector('.red-message').innerText = 'Wrong password'
    }
    console.log(password.value, passwordConfirm.value)
});